public static Builder builder() {
	return new Builder();
}

public static class Builder {

	private Builder() {}

	public ${enclosing_type} build() {
		return new ${enclosing_type}();
	}
}
